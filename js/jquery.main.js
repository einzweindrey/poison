$(function(){

    $("a.popup").fancybox({
        'transitionIn'  :   'elastic',
        'transitionOut' :   'elastic',
        'speedIn'       :   600,
        'padding'       : 20,
        'minWidth'      :   400,
        'maxWidth'      :   400,
        scrolling: false,
        'scrollOutside': false,
        helpers: {
                overlay: {
                locked: false
                }
            },

        beforeClose: function(){
            $('html').removeClass('fancybox')
        }

    });

    $("a[rel=group]").fancybox({
        'transitionIn'		: 'none',
        'transitionOut'		: 'none',
        'titlePosition' 	: 'over',
        scrolling: false,
        'scrollOutside': false,
        helpers: {
            overlay: {
                locked: false
            }
        },
        beforeClose: function(){
            $('html').removeClass('fancybox')
        }

    });

    $('.popup').click(function (){
        $('html').addClass('fancybox')
    });

    $(".sertificate__pic a").click(function (){
        $('html').addClass('fancybox')
    })

} );

var Shablon = function (obj) {
    this.obj = obj;


    this.init();
};
Shablon.prototype = {
    init: function () {
        var self = this;

        self.core = self.core();
        self.core.build();
    },
    core: function () {
        var self = this;

        return {
            addEvents: function () {

            },
            build: function () {
                self.core.addEvents();
            }
        };
    }
};

$(window).on({
    load: function () {

    }
});
