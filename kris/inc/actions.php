<?php

remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'noindex');
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'signuppageheaders');

add_filter('widget_text', 'do_shortcode');
add_filter('wpcf7_form_elements', 'do_shortcode');

if (function_exists('acf_add_options_page')) {
    acf_add_options_page();
}


add_action('after_setup_theme', 'theme_setup');
function theme_setup()
{
    add_theme_support('menus');
    add_theme_support('post-thumbnails');
//    add_image_size('1378x440', 1378, 440, true);
}

add_action('wp_enqueue_scripts', 'add_js');
function add_js()
{

    wp_enqueue_style('site-style', TEMPLATEURI . '/css/main.css');
    wp_enqueue_style('site-fan', TEMPLATEURI . '/css/jquery.fancybox.css');

    wp_deregister_script('jquery');
    wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js', false, '2.1.3');
    wp_enqueue_script('jquery');


}

