<!--site__footer-->
<footer class="site__footer">

    <div class="site__footer-layout">

        <!--footer-logo-->
        <a href="/" class="footer-logo">
            <img src="<?php echo TEMPLATEURI; ?>/img/footer-logo.png" alt="AgroClean">
            2015 © Copyright AgroClean
        </a>
        <!--/footer-logo-->

        <!--footer-callback-->
        <div class="footer-callback">
            <a href="mailto:rats@gmail.com">rats@gmail.com</a>
            <a href="tel:+380691923403">+38 (069) 192-34-03</a>
            <a class="popup" href="#callback">Перезвоните мне</a>
        </div>
        <!--/footer-callback-->

    </div><!-- /site__footer-layout -->

</footer>
<!-- /site__footer -->

</div><!-- /site -->

<div style="display:none">

    <div class="order" id="order">
        <form action="#">

            <h3>Оставить заявку</h3>

            <input type="text" placeholder="Имя*">
            <input type="email" placeholder="Телефон или Email*">
            <textarea name="message"  placeholder="Сообщение*"></textarea>
            <button class="btn" type="submit">Отправить</button>

            <!--order__callback-->
            <div class="order__callback">
                <p>Наш менеджер свяжется с вами в ближайшее рабочее время</p>
            </div>
            <!--/order__callback-->

        </form>
    </div>

    <div class="order" id="callback">
        <form action="#">

            <h3>Заказать обратный звонок</h3>

            <input type="text" placeholder="Имя*">
            <input type="email" placeholder="Телефон*">
            <button class="btn" type="submit">Заказать</button>

            <!--order__callback-->
            <div class="order__callback">
                <p>Наш менеджер свяжется с вами в ближайшее рабочее время</p>
            </div>
            <!--/order__callback-->

        </form>
    </div>

</div>


<script src="<?php echo TEMPLATEURI ?>/js/jquery.main.js"></script>
<script src="<?php echo TEMPLATEURI ?>/js/jquery.fancybox.js"></script>

<?php wp_footer(); ?>
</body>
</html>
