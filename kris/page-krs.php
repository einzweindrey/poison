<?php
/**
 * Template Name: Крысиная смерть
 */
?>

<?php get_header(); ?>
    <!--site__content-->
    <div class="site__content">

        <!--about-->
        <div class="about">

            <!--about__wrap-->
            <div class="site__content__wrap about__wrap">

                <h2 class="site__title">О препарате</h2>

                <p>
                    <img src="pic/ks1.jpg" width="219" height="225" alt="AgroClean">
                    Cредство от грызунов «Крысиная смерть №1» — уникальный препарат на основе яда
                    пролонгированого действия произвоства итальянской компании «Activa S.r.l». Действие
                    приманки проявляется на 3–4-й день и выражается в том, что грызуны испытывают удушье,
                    начинается внутренеее кровотечение, от которого на 4–8-й день наступает гибель животных.
                </p>

                <p>
                    «Крысиная смерть №1» не вызывает острого пищевого отравления, поэтому грызуны не могут
                    определить причину недомогания и предупредить популяцию об опасности. Из-за испытываемого
                    удушья грызуны выходят из укрытий и погибают не в норах, под полами, а за пределами
                    помещения, на открытом месте.
                </p>

                <p>
                    Оригинальная препаративная форма не имеет аналагов в Украине, в восточной Европе и является
                    основной в Италии, Франции, Германии и США.
                </p>

            </div>
            <!--/about__wrap-->

        </div>
        <!--/about-->

        <!--advantages-->
        <div class="advantages">

            <!--advantages__wrap-->
            <div class="site__content__wrap advantages__wrap">

                <h2 class="site__title">Преимущества</h2>

                <ul>
                    <li>
                        <span>Экологически безопасный продукт</span>
                        Изготовлен из натуральных ингредиентов: муки, сахара и растительного масла.
                    </li>
                    <li>
                        <span>Привлекателен для грызунов</span>
                        Благодаря натуральному составу предпочтение отдается приманке, а не обычной пище.
                    </li>
                    <li>
                        <span>Эффективен при одноразовом поедании</span>
                        Грызуну достаточно единоразово попробовать яд и его смерть гарантирована.
                    </li>
                    <li>
                        <span>Нет необходимости в специальной утилизации</span>
                        Остатки препарата разлагаются в течение 30 дней.
                    </li>
                    <li>
                        <span>Не нужно искать трупы грызунов</span>
                        По причине удушья крысы выбираются из помещения, где и происходит их гибель.
                    </li>
                    <li>
                        <span>Срок годности — 24 месяца</span>
                        Не теряет эффективности благодаря противомикробным и антигрибковым ингредиентам.
                    </li>
                </ul>

            </div>
            <!--/advantages__wrap-->

        </div>
        <!--/advantages-->

        <!--certificate-->
        <div class="certificate">

            <!--certificate__wrap-->
            <div class="site__content__wrap certificate__wrap">

                    <span>Средство «Крысиная смерть №1» официально зарегистрировано
                        в Украине, России, Беларуси, Казахстане, Азербайджане и Молдове</span>

                <!--sertificate__pic-->
                <div class="sertificate__pic">
                    <a href="pic/sertificate-big.jpg" rel="group">
                        <img src="pic/certificate.jpg" alt="certificate">
                    </a>
                    <a href="pic/sertificate-big.jpg" rel="group">
                        <img src="pic/certificate.jpg" alt="certificate">
                    </a>
                    <a href="pic/sertificate-big.jpg" rel="group">
                        <img src="pic/certificate.jpg" alt="certificate">
                    </a>
                </div>
                <!--/sertificate__pic-->

            </div>
            <!--/certificate__wrap-->

        </div>
        <!--/certificate-->

        <!--new-order-->
        <div class="new-order">

            <!--new-order__wrap-->
            <div class="site__content__wrap new-order__wrap">
                <span>Оставьте заявку и мы поможем решить проблему с грызунами</span>

                <!--btn-->
                <a href="#order" class="btn popup">Оставить заявку</a>
                <!--/btn-->
            </div>
            <!--/new-order__wrap-->

        </div>
        <!--/new-order-->

        <!--about-work-->
        <div class="about-work">

            <!--about-work__wrap-->
            <div class="site__content__wrap about-work__wrap">

                <h2 class="site__title">Как мы работаем</h2>

                <ul>
                    <li class="order">
                        Вы отправляете заявку
                        и мы связываемся с вами
                        в течение рабочего дня
                    </li>
                    <li class="cash">
                        Согласовываем способ
                        оплаты и доставки
                    </li>
                    <li class="no-rat">
                        Вы получаете препарат и
                        в течение 8 дней полностью избавляетесь от грызунов
                    </li>
                    <li class="idea">
                        Определяем масштабы проблемы
                        и предлагаем её решение
                    </li>
                    <li class="delivery">
                        Отгружаем продукцию в течение рабочего дня с момента оплаты
                    </li>
                </ul>

            </div>
            <!--/about-work__wrap-->

        </div>
        <!--/about-work-->

        <!--why-us-->
        <div class="why-us">

            <!--why-us__wrap-->
            <div class="site__content__wrap why-us__wrap">

                <h2 class="site__title">Почему мы</h2>

                <ul>
                    <li>
                        Наша компания — официальный дилер препарата «Крысиная смерть №1»
                    </li>
                    <li>
                        Любые объемы препарата всегда есть
                        в наличии на наших складах
                    </li>
                    <li>
                        Гарантируем низкую стоимость благодаря прямым поставкам продукции
                    </li>
                    <li>
                        Экономим ваше время — отгрузка
                        в течение 12 часов с момента оплаты
                    </li>
                    <li>
                        Широкая география: работаем по всей Украине и странам СНГ
                    </li>
                    <li>
                        Подробно консультируем о препарате «Крысиная смерть №1» и его действии
                    </li>
                </ul>

            </div>
            <!--/why-us__wrap-->

        </div>
        <!--/why-us-->

        <!--new-order-->
        <div class="new-order">

            <!--new-order__wrap-->
            <div class="site__content__wrap new-order__wrap">
                <span>Оставьте заявку и мы поможем решить проблему с грызунами</span>

                <!--btn-->
                <a href="#order" class="btn popup">Оставить заявку</a>
                <!--/btn-->

            </div>
            <!--/new-order__wrap-->

        </div>
        <!--/new-order-->

    </div>
    <!-- /site__content -->
<?php get_footer(); ?>