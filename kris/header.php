<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8" />
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=1024">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">

    <title><?php document_title(); ?></title>
    <?php wp_head(); ?>

</head>

<body>

<div class="site">
<?php if (is_page() || is_single() || is_singular() || is_404()) {
    the_post();
}; ?>
        <!--site__header-->
        <header class="site__header">

            <!--site__header__wrap-->
            <div class="site__header__wrap">

                <!--logo-->
                <h1 class="logo">
                    <img src="<?php echo TEMPLATEURI; ?>/img/logo.png" alt="AgroClean">
                    Ваш помощник по борьбе с вредителями
                </h1>
                <!-- /logo -->

                <!--header-callback-->
                <div class="header-callback">
                    <a href="mailto:rats@gmail.com">rats@gmail.com</a>
                    <a href="tel:+380691923403">+38 (069) 192-34-03</a>
                    <a class="popup" href="#callback">Перезвоните мне</a>
                </div>
                <!--/header-callback-->

                <!--header-info-->
                <div class="header-info">

                    <!--header-info__attention-->
                    <span class="header-info__attention">Завелись грызуны?</span>
                    <!--/header-info__attention-->

                    <span>Традиционные средства не избавляют от этой напасти?</span>
                    <p>
                        Препарат «Крысиная смерть №1» избавит вас от крыс
                        и мышей без лишних хлопот!
                    </p>

                    <div>
                        Оставьте заявку и мы поможем решить проблему с грызунами
                        <a class="btn popup" href="#order">Оставить заявку</a>
                    </div>

                    <p class="header-info__text">
                        Оставьте заявку и мы поможем решить проблему с грызунами и бла - бла - бла
                    </p>

                </div>
                <!--/header-info-->

            </div>
            <!--/site__header__wrap-->

        </header>
        <!-- /site__header -->
