<?php
/**
 * Template Name: Home Page
 */
?>

<?php get_header(); ?>
    <!-- desktop-slide -->
    <div class="desktop-slide">
        <?php
        if (have_rows('gallery_list'))
            while (have_rows('gallery_list')) {
                the_row();
                echo wp_get_attachment_image(get_sub_field('image_sl'), '1378x440', false, array('alt'=>'picture'));
            }
        ?>
        <div class="desktop-slide__prev"></div>
        <div class="desktop-slide__next"></div>
    </div>
    <!-- /desktop-slide -->
    <div class="site__content">

        <!-- index-mobile -->
        <div class="index-mobile">
            <?php
            if (have_rows('images_m'))
                while (have_rows('images_m')) {
                    the_row();
                    echo '<div href="#" class="index-mobile__item"><img src="' . get_sub_field('image_mob') . '" alt="arnon-briner"/></div>';
                }
            ?>
        </div>

        <!-- index-desktop -->
        <div class="index-desktop">

            <!-- pictures -->
            <div class="pictures">

                <!-- pictures__title -->
                <h2 class="pictures__title">
                    <img src="<?php echo get_field('title_image'); ?>" alt=""/>
                </h2>
                <!-- /pictures__title -->

                <!-- pictures__list -->
                <div class="pictures__list">

                    <?php
                    if (have_rows('list_pages'))
                        while (have_rows('list_pages')) {
                            the_row();
                            echo '<a href="' . get_sub_field('page_url') . '" class="pictures__item">
                        <img src="' . get_sub_field('image_category') . '" width="225" height="150" alt="picture"/>
                        <span class="pictures__text centering">
                            <span>
                                <img src="' . get_sub_field('title_image') . '" alt=""/>
                            </span>
                        </span>
                    </a>
                    ';
                        }
                    ?>

                </div>
                <!-- /pictures__list -->

            </div>
            <!-- /pictures -->

        </div>
        <!-- /index-desktop -->
        <!-- /index-mobile -->
    </div><!-- /site__content -->
<?php get_footer(); ?>