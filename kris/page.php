<?php

get_header(); ?>
<!-- site__content -->
<div class="site__content">

    <!-- about -->
    <section class="about">
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
    </section>
    <!-- /about -->


</div><!-- site__content -->

<?php get_footer(); ?>
